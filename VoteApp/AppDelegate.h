//
//  AppDelegate.h
//  VoteApp
//
//  Created by paparotnick on 23.06.15.
//  Copyright (c) 2015 octabrain. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

