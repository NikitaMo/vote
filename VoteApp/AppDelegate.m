//
//  AppDelegate.m
//  VoteApp
//
//  Created by paparotnick on 23.06.15.
//  Copyright (c) 2015 octabrain. All rights reserved.
//

#import "AppDelegate.h"
#import "VTRestKitClient.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
#pragma unused(application)
#pragma unused(launchOptions)
    [self settingUpRestKit];//set up restkit on launch application
    [Fabric with:@[CrashlyticsKit]];
    return YES;
}

-(void)settingUpRestKit
{
    [RESTKITCLIENT setupRestKit];
}

- (void)applicationWillResignActive:(UIApplication *)application {
#pragma unused(application)
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
#pragma unused(application)
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
#pragma unused(application)
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
#pragma unused(application)
}

- (void)applicationWillTerminate:(UIApplication *)application {
#pragma unused(application)    
}



@end
