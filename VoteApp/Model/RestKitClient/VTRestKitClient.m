//
//  VTRestKitClient.m
//  VoteApp
//
//  Created by paparotnick on 23.06.15.
//  Copyright (c) 2015 octabrain. All rights reserved.
//

#import "VTRestKitClient.h"

@interface VTRestKitClient()

@end

@implementation VTRestKitClient

+ (id)sharedInstance {
    static dispatch_once_t p = 0;
    __strong static VTRestKitClient * _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

+ (RKObjectManager *)manager
{
    return [RKObjectManager sharedManager];
}

#pragma mark -mapping

//create all mapping files here
-(void)firstMapping
{
}

#pragma mark -ResponseDescr

//add all descriptors here
-(void)addAllResponseDescr
{
    
}


//simly method for add response descrirtors in RestKit mapping
-(void)addResponseDescrForMapping:(RKEntityMapping*)mapping keyPath:(NSString*)keyPayth pathPattern:(NSString*)pathPattern method:(RKRequestMethod)method statusCode:(RKStatusCodeClass)statusCode
{
    [self.manager addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:mapping method:method pathPattern:pathPattern keyPath:keyPayth statusCodes:RKStatusCodeIndexSetForClass(statusCode)]];
}

#pragma mark -SetupRK

- (void)setupRestKit
{
    NSURL *baseURL = [NSURL URLWithString:@"http://example.ru"]; // set base URL here
    RKObjectManager *managerRest = [RKObjectManager managerWithBaseURL:baseURL];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    managerRest.managedObjectStore = managedObjectStore;
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    
    [managedObjectStore createPersistentStoreCoordinator];
    NSError *error1;
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"VoteStorage.sqlite"];
    
    NSError *error;
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil  withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    if (!persistentStore) {
        [[NSFileManager defaultManager] removeItemAtPath:storePath error:&error];
        NSLog(@"Deleted old database %@, %@", error1, [error1 userInfo]);
        persistentStore=[managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil  withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES} error:&error];
    }
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    [self firstMapping];
    [self addAllResponseDescr];
    
}


@end
