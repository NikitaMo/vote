//
//  VTRestKitClient.h
//  VoteApp
//
//  Created by paparotnick on 23.06.15.
//  Copyright (c) 2015 octabrain. All rights reserved.
//


#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>

#define RESTKITCLIENT [VTRestKitClient sharedInstance]

@interface VTRestKitClient : NSObject

//singleton
+ (id)sharedInstance;

//manager to manage requests
@property (strong,nonatomic) RKObjectManager *manager;

//first setup RestKit
- (void)setupRestKit;

@end
