# README #

### Vote ios Application ###
* Version 1.0

* Quick summary

**design and info**

https://drive.google.com/folderview?id=0BwqYPTwFh7_Ifk5iRGQ3eVFWNzRlU3N5YUttTkZBVDM0bExOazhWTkM3T3RuT0VVY3lzQk0&usp=sharing

**grafic**
https://docs.google.com/spreadsheets/d/1rKeeWk5wu6vx-tSn46hFA8xTM8KVDaNP3jbV2_bwyYg/edit#gid=1168933716

**api doc**

https://docs.google.com/document/d/1pIxgnUr12m4IqBSErIia_fvd00rdDA0GqhIa8vzreKg/edit#heading=h.7k73r0qdrqkw

### How do I get set up? ###

* Summary of set up

    1. clone repository and update pods
    2. checkout local your feature branch

* Configuration
* Libraries:

    **RestKit** - for network operation and mapping to database [https://github.com/RestKit/RestKit]()

    **RESideMenu** - for left-side menu [https://github.com/romaonthego/RESideMenu]()

    **DateTools** - tool for managment date [https://github.com/MatthewYork/DateTools]()

    **Masonry** - tool for simple managment autolayot in code [https://github.com/SnapKit/Masonry]()


* Database configuration
    Core data with RestKit


### Contribution guidelines ###

* Code review
[https://docs.google.com/document/d/1eFT-etVEuZHDsDiSWOPF8dlMzU7vPfvTgv5QAXPMrR8/edit]()

* Code style iOS:
[https://docs.google.com/document/d/1yTq9dKZH_wZ9QQckxjYmUSUAc3VR631L30X-_FG9Oo4/edit?usp=sharing]()